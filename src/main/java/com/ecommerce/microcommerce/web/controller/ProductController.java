package com.ecommerce.microcommerce.web.controller;

import com.ecommerce.microcommerce.dao.ProductDao;
import com.ecommerce.microcommerce.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Combinaison de Controller (class level annotation) & ResponseBody (method level annotation)
// L'annotation ResponseBody est associée à des méthodes qui devront répondre directement sans passer par une vue
@RestController
public class ProductController {

    @Autowired
    private ProductDao productDao;

/*    //Faire le lien entre l'URI "/Produits invoqué par GET et al émthode Java
    @RequestMapping(value="/Produits", method=RequestMethod.GET)
    public String listeProduits(){
        return "un exemple de produit";
    }*/

/*
    //@RequestMapping(value="/Produits/{id}", method = RequestMethod.GET)
    @GetMapping(value="/Produits/{id}")
    public String afficherUnProduit(@PathVariable int id) {
        return "Vous avez demandé le produit dont l'id "+ id;
    }
*/


/*    //@RequestMapping(value="/Produits/{id}", method = RequestMethod.GET)
    @GetMapping(value="/Produits/{id}")
    public Product afficherUnProduit(@PathVariable int id) {
        return new Product(id, "aspirateur", 250);
    }*/

    //Récupérer la liste des produits
    @RequestMapping(value="/Produit", method=RequestMethod.GET)
    public List<Product> listeProduits() {
        System.out.println("Get ALL called");
        return productDao.findAll();
    }

    //Récupérer un produit par son Id
    //@GetMapping(value="/Produit/{id}")
    @RequestMapping(value="/Produit/{id}", method = RequestMethod.GET)
    public Product afficherUnProduit(@PathVariable int id) {
        System.out.println("Get {id} called");
        return productDao.findById(id);
    }

    @GetMapping(value="/Produit/{id}/{name}")
    public Product afficherUnProduit(@PathVariable int id, @PathVariable String name) {
        System.out.println("Get {id}/{name} called");
        return productDao.findById(id);
    }

    // it does not work! why?
//    @RequestMapping(path="/Produit", method = RequestMethod.GET)
//    public Product displayProduct(@RequestParam int id) {
//        return productDao.findById(id);
//    }

    // The RequestBody annotation indicates the spring should convert
    //the param received from JSON format to a java Object
    @PostMapping(value="/Produit")
    public void ajouterProduit(@RequestBody Product produit){
        System.out.println("Post called");
        productDao.save(produit);
    }
}